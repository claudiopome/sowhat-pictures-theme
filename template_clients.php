<?php
/*
Template Name: Page - Clients
*/
?>
<?php get_header(); ?>
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="projects-home">
						
					
					<ul class="projects-list">
					<?php
						$args = array(
							'post_type' => 'clients',
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'ASC'
						);
						$clients = new WP_Query($args);
						while( $clients -> have_posts() ) : $clients -> the_post();
						$post_image = sowhatpictures_theme_fetch_post_image(); 
					?>
						<li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="<?php echo $post_image; ?>" alt="Project">
								</div>
						</li>
					<?php endwhile; ?>
					</ul>
				</section>
				
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>Wanna be one of them?</h1>
							<ul class="button-list">
								<li>
									<a href="mailto:crew@sowhatpictures.com" class="btn">Let's work together</a>
								</li>
								
							</ul>
						
					</div>
				</section>		
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>