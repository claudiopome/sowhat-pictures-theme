<?php
/*
Template Name: Page - Home
*/
?>
<?php get_header(); ?>
		
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="projects-home">
					
					<ul class="projects-list">
						
					<li class="projects__item grid__item ">
						<a href="http://localhost:8888/sowhatpictures/showreel-2015" class="projects__link">
							<div class="projects__content">
								<img src="img/thumbnails/showreel.png" alt="Project">
							
							<div class="projects__overlay">
									<div class="projects__heading">
<h6 class="projects__cat separator">Featured</h6>
										<h1 class="projects__title">Showreel 2015</h1>
											
									</div>
								</div>
							</div>
						</a>
					
					</li><li class="projects__item grid__item one-half">
						<a href="http://localhost:8888/sowhatpictures/production/black" class="projects__link">
							<div class="projects__content">
								<img src="img/thumbnails/production/black.png" alt="Project">
							
							<div class="projects__overlay">
									<div class="projects__heading">
										<h6 class="projects__cat separator">Latest Project</h1>
											<h1 class="projects__title">Black</h1>
									</div>
								</div>
							</div>
						</a>
					</li><li class="projects__item grid__item one-half">
						<a href="http://localhost:8888/sowhatpictures/production/life-in-riva" class="projects__link">
							<div class="projects__content">
								<img src="img/thumbnails/production/life-in-riva.png" alt="Project">
							
							<div class="projects__overlay">
									<div class="projects__heading">
										<h6 class="projects__cat separator">Latest Project</h1>
											<h1 class="projects__title">Life in Riva</h1>
									</div>
								</div>
							</div>
						</a>
					</li>
					</ul>
				</section>
				<section class="section-intro section--black text-center">
					<h6 class="separator">Who we are</h6>
					<h1>So What is a production company founded by three independent professionals.</h1>
				</section>
				<section class="section-bottom section-clients section--white text-center ">
					<div class="inner-bottom">
						<header class="section-bottom__heading">
							<h6 class="separator">Our Clients</h6>
							<h1>Who we worked with</h1>
						</header>
						<section class="clients-list">
							<ul>
								<li class="clients__item fox two-sixth">Fox</li>
								<li class="clients__item universal two-sixth">Universal</li>
								<li class="clients__item barilla two-sixth">Barilla</li>
								<li class="clients__item pfizer two-sixth">Pfizer</li>
								<li class="clients__item repubblica two-sixth">Repubblica</li>
								<li class="clients__item alitalia two-sixth">Alitalia</li>
								<li class="clients__item leoburnett two-sixth">Leo Burnet</li>
								<li class="clients__item nationalgeographic two-sixth-last">National Geographic</li>
							</ul>
							
						</section>
						
					</div>
				</section>
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>Wanna know more about us?</h1>
							<ul class="button-list">
								<li>
									<a href="<?php bloginfo('url'); ?>/about" class="btn">Keep reading</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/projects" class="btn">More projects</a>
								</li>
								
							</ul>
						
					</div>
				</section>		
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>