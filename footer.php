		<footer class="site-footer">
			<div class="inner-footer">
				<!--
				<section class="site-footer__entry two-cols">
					<h1 class="site-footer__logo">So What</h1>
				</section>
				
				<section class="site-footer__entry six-cols">
					
					<h3>SOWHAT PICTURES</h3>
					<p>Via Placido Zurla 49/B 00176 Rome (Italy)<br><a href="tel:+39 3393967086">+39 3393967086</a><br><a href="mailto:crew@sowhatpictures.com">crew@sowhatpictures.com</a><br>P.IVA/VAT 11873451006</p>
				</section>
				-->
				<!--
				<section class="site-footer__entry site-footer--nav four-cols">
					<nav>
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Projects</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</nav>
				</section>
				-->
				<div class="site-footer__entry four-cols">
					<?php /* Widget Area */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Where we are')); ?>
					
				</div>
				
				<div class="site-footer__entry four-cols">
					<?php /* Widget Area */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Email us')); ?>
				</div>
				
				<div class="site-footer__entry four-cols">
					<?php /* Widget Area */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Call')); ?>
				</div>
				
				<div class="site-footer__entry four-cols--last">
					<ul class="site-footer--social">
						<?php /* Widget Area */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Be Social')); ?>						
					</ul>
				</div>
			</div>
		</footer>
		<section class="copyright text-center"><small>&copy <?php echo date('Y'); ?> So What Pictures P.IVA/VAT 11873451006 Sede Legale Via Placido Zurla 49B 00176 &ndash; Roma</small></section>
		
		<?php wp_footer(); ?>
		<script>
			
			$(document).ready(function() {
				
				$("#owl-demo").owlCarousel({
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem : true,
					autoPlay : true
				});
				
			});
			
			
		</script>
	</body>
</html>