<?php
/*
Template Name: Page - About
*/
?>
<?php get_header(); ?>
<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="section-about">
						<header class="section-intro section--black text-center">
							<h6 class="separator">Who we are</h6>
							<h1>We offer to our clients creativity and innovation.<br> We support and grow our projects as we develop and make our clients projects unique. 
</h1>
						</header>
					<ul class="team-list">
					<li class="team__item grid__item one-third">
							
								<img src="<?php bloginfo('template_directory');?>/img/lorenzo.jpg" alt="Project">
							
							<div class="team__overlay">
								<div class="team__content">
									<h6 class="separator">Filmmaker, Creative Producer</h1>
									<h2 class="team__title">Lorenzo Giordano</h2>
									<!--
									<ul class="team__social">
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-envelope"></span>
											</a>
										</li>
										
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-phone"></span>
											</a>
										</li>	
									</ul>
									-->
								</div>
							</div>
					
					</li><li class="team__item grid__item one-third">
							
								<img src="<?php bloginfo('template_directory');?>/img/toto.jpg" alt="Project">
							
							<div class="team__overlay">
								<div class="team__content">
									<h6 class="separator">Director, Editor</h1>
									<h2 class="team__title">Ippolito Simion</h2>
									<!--
									<ul class="team__social">
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-envelope"></span>
											</a>
										</li>
										
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-phone"></span>
											</a>
										</li>	
									</ul>
									-->
								</div>
							</div>
					</li><li class="team__item grid__item one-third">
							
								<img src="<?php bloginfo('template_directory');?>/img/sara.jpg" alt="Project">
							
							<div class="team__overlay">
								<div class="team__content">
									<h6 class="separator">Motion director</h1>
									<h2 class="team__title">Sara Taigher</h2>
									
									<!--
									<ul class="team__social">
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-envelope"></span>
											</a>
										</li>
										
										<li>
											<a href="#" target="_blank">
												<span class="fa fa-phone"></span>
											</a>
										</li>	
											
									</ul>
									-->
								</div>
							</div>
					</li>
					</ul>
				</section>
				
				
				<header class="section-intro section--white text-center">
					<h6 class="separator">Where we work</h6>
					<h1>In 2016 we moved to a 300 square meters loft in Rome. We share the studio with <a style="border-bottom: 4px solid #51d9e9;" href="http://none.business" target="_blank">NONE collective</a>, curating its moving image communication in the form of video reports and animations.</h1>
				</header>
				
				<img src="<?php bloginfo('template_directory'); ?>/img/studio.jpg" alt="studio">
				
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>Let's talk about your project</h1>
							<ul class="button-list">
								<li>
									<a href="mailto:crew@sowhatpictures.com" class="btn">Contact us</a>
								</li>
								
							</ul>
						
					</div>
				</section>	
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>