<?php
/*
Template Name: Page - Reel
*/
?>
<?php get_header(); ?>
		
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="projects-home">
				
						<header class="section-intro--small section--black text-center">
							
							<h6 class="separator">Reel</h6>
							
							<h1>Showreel</h1>
						
						
						<div class="projects__container">
						<?php 
							
							$args = array(
								'post_type' => 'projects',
								'tax_query' => array(
									array(
										'taxonomy' => 'projects_cat',
										'field'    => 'slug',
										'terms'    => 'reel',
										),
									),
							);
							$query = new WP_Query( $args );
							?>
							
							<?php if ( $query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); 
							
								
								if (get_post_meta($post->ID, 'link-video', true )) {
									echo '<iframe src="' . get_post_meta($post->ID, 'link-video', true) . '?title=0&byline=0&portrait=0" style="background-color:#000" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								}
								else {
									
									echo "We're sorry but we couldn't find any content";
								}
							?>
						</div>
		
				</section>
				</div>
				<?php endwhile; endif; ?>
				<div class="share-widget">
					<ul>
						<?php
							$sowhat_post_url = get_permalink();
							$sowhat_post_title = str_replace( ' ', '%20', get_the_title());
							$sowhat_post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
							
							$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$sowhat_post_url;
							$twitterURL = 'https://twitter.com/intent/tweet?text='.$sowhat_post_title.'&amp;url='.$sowhat_post_url.'&amp;via=SoWhatPictures';
							$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$sowhat_post_url.'&amp;media='.$sowhat_post_image[0].'&amp;description='.								$sowhat_post_title;

							$googleURL = 'https://plus.google.com/share?url='.$sowhat_post_url;
						?> 
						<li class="share-widget__option share-widget--facebook"><a href="<?php echo $facebookURL ?>" target="_blank"><span>Facebook</span></a></li>
						<li class="share-widget__option share-widget--twitter"><a href="<?php echo $twitterURL ?>" target="_blank"><span>Twitter</span></a></li>
						<li class="share-widget__option share-widget--pinterest"><a href="<?php echo $pinterestURL ?>" target="_blank"><span>Pinterest</span></a></li>
						<li class="share-widget__option share-widget--google-plus"><a href="<?php echo $googleURL ?>" target="_blank"><span>Google+</span></a></li>
					</ul>
				</div>
				
				<nav class="section-intro--small post-navigation text-center section--white">
					<ul class="post-navigation__list">
						<li class="post-navigation__item post-navigation__item--prev">
							<?php next_post_link('%link', '<span class="post-navigation__icon fa fa-angle-left fa-3x"></span><span class="post-navigation__text">Previous project</span>'); ?>
						</li><li class="post-navigation__item post-navigation__item--all">
							<a href="<?php bloginfo('url'); ?>/projects">
								<span class="post-navigation__icon fa fa-th-large fa-3x"></span>
								<span class="post-navigation__text">All projects</span>
							</a>
						</li><li class="post-navigation__item post-navigation__item--prev">
							<?php previous_post_link('%link', '<span class="post-navigation__icon fa fa-angle-right fa-3x"></span><span class="post-navigation__text">Next project</span>'); ?>
						</li>
					</ul>
				</nav>
				
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>We'd love to work for you</h1>
							<ul class="button-list">
								<li>
									<a href="mailto:crew@sowhatpictures.com" class="btn">Hire us</a>
								</li>
								
							</ul>
						
					</div>
				</section>		
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
		
<?php get_footer(); ?>