<section class="share-widget">
	<!--
	<ul class="share-widget__options">
		<li class="share-widget__option"><h6>Share</h6></li>
		<li class="share-widget__option share-widget--facebook"><a href="#"><span>Facebook</span></a></li>
		<li class="share-widget__option share-widget--twitter"><a href="#"><span>Twitter</span></a></li>
		<li class="share-widget__option share-widget--pinterest"><a href="#"><span>Pinterest</span></a></li>
		<li class="share-widget__option share-widget--google-plus"><a href="#"><span>Google Plus</span></a></li>
	</ul>
	-->
	
	<div class="share-widget__heading">
		<h6>Share</h6>
	</div>
	<div class="share-widget__options">
		<ul>
			<li class="share-widget__option share-widget--facebook"><a href="#"><span>Facebook</span></a></li>
			<li class="share-widget__option share-widget--twitter"><a href="#"><span>Twitter</span></a></li>
			<li class="share-widget__option share-widget--pinterest"><a href="#"><span>Pinterest</span></a></li>
			<li class="share-widget__option share-widget--google-plus"><a href="#"><span>Google+</span></a></li>
		</ul>
	</div>
</section>

