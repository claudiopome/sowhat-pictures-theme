<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php sowhatpictures_theme_display_titles(); ?></title>
		<script src="//use.typekit.net/qmw4ltu.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>">
		<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/owl-carousel/owl.theme.css">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<script>
			$(document).ready(function() {
				$('.projects__container').fitVids();
				
				
				
			});
		</script>
		<?php wp_head(); ?>
	</head>
	
	<body>
		<header class="site-header clearfix" role="banner">
			<div class="inner-header">
				<div class="logo-container">
					<a href="<?php bloginfo('url');?>"><h1><?php bloginfo('name');?></h1></a>
				</div>
				<nav id="menu" role="navigation">
					<?php sowhatpictures_theme_custom_nav_menu(); ?>
				</nav>
			</div>
		</header>