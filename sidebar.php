<aside class="main-sidebar grid__col--2-8">	
		
	<section class="main-sidebar__block main-sidebar--news">
		<header class="main-sidebar__header header--news">
			<h3>Articoli Recenti</h3>
		</header>
			<ul class="news-list">
				<li class="event-list__item">
						<figure>
							<span data-picture data-alt="Thumbnail 1">
								<span data-src="img/thumbnails/thumb-1-small.jpg"></span>
								<span data-src="img/thumbnails/thumb-1-medium.jpg" data-media="(min-width: 37em)"></span>
								<span data-src="img/thumbnails/thumb-1-large.jpg" data-media="(min-width: 50em)"></span>
	
								<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
								<noscript><img src="img/thumbnails/thumb-1-small.jpg" alt="Thumbnail 1"></noscript>
							</span>
						</figure>
						<header>
							<h4 class="event-list__title"><a href="index2.php">EDL supporters attack police during Rotherham sex abuse protest</a></h4>
							<p class="event-list__date">02 Maggio 2014</a></p>
						</header>
				</li>
							
				<li class="event-list__item">
					<figure>
							<span data-picture data-alt="Thumbnail 2">
								<span data-src="img/thumbnails/thumb-2-small.jpg"></span>
								<span data-src="img/thumbnails/thumb-2-medium.jpg" data-media="(min-width: 37em)"></span>
								<span data-src="img/thumbnails/thumb-2-large.jpg" data-media="(min-width: 50em)"></span>
	
								<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
								<noscript><img src="img/thumbnails/thumb-2-small.jpg" alt="Thumbnail 2"></noscript>
							</span>
					</figure>
					<header>
						<h4 class="event-list__title"><a href="index.php">Guinea faces Ebola epidemic on unprecedented scale, doctors warn</a></h4>
						<p class="event-list__date">30 Aprile 2014</p>
					</header>
				</li>
							
				<li class="event-list__item">
					<figure>
							<span data-picture data-alt="Thumbnail 3">
								<span data-src="img/thumbnails/thumb-3-small.jpg"></span>
								<span data-src="img/thumbnails/thumb-3-medium.jpg" data-media="(min-width: 37em)"></span>
								<span data-src="img/thumbnails/thumb-3-large.jpg" data-media="(min-width: 50em)"></span>
	
								<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
								<noscript><img src="img/thumbnails/thumb-3-small.jpg" alt="Thumbnail 3"></noscript>
							</span>
					</figure>
					<header>
						<h4 class="event-list__title"><a href="index3.php">We don't see ourselves as underdogs. We are Manchester United – Giggs</a></h4>
						<p class="event-list__date">30 Aprile 2014</p>
					</header>
				</li>
			</ul>
			
	</section>		
					
						
</aside> <!-- /2-8 -->
