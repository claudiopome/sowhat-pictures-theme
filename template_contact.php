<?php
/*
Template Name: Page - Contact
*/
?>
<?php get_header(); ?>		
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="section-contact">
						<header class="section-intro section--black text-center">
							<h6 class="separator">Shoot us an email</h6>
							<h1><a href="mailto:info@sowhatpictures.com">info(at)sowhatpictures.com</a></h1>
						</header>
				</section>
				
				
				<section class="section--white">
					<div class="contact-hq grid__item text-center one-half">
						<span>
							<h6 class="separator">Rome office</h6>
							<p>Via Giuseppe Libetta 21<br>
							Interno 2, 00154<br>
							Rome, Italy
							</p>	
							
							<ul class="button-list">
								<li>
									<a href="https://goo.gl/maps/m4DPRCXzZwk" class="btn btn--blue" target="_blank">See on maps</a>
								</li>
								
							</ul>
						</span>
					</div><div class="grid__item map-container one-half">
						<div id="map-rome"></div>
					</div>
				</section>
				
										
			</section> <!--/grid -->
		</main>
		
		<script>
			var map;
			jQuery(function($) {
				$(document).ready(function() {
					function initialize() {
						var latlng = new google.maps.LatLng(41.8633020, 12.4802700);
						var myOptions = {
							center: latlng,
							zoom: 15, 
							zoomControl: true,
					    zoomControlOptions: {
					      style: google.maps.ZoomControlStyle.SMALL
					    },
							panControl : false,
							scrollwheel: false,
							mapTypeControl: false,
							scaleControl: false,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP,
						};
						map = new google.maps.Map(document.getElementById("map-rome"), myOptions);
						
						// Map Marker
						var marker = new google.maps.Marker({
							map: map,
							draggable: false,
							position: latlng,
							icon: "http://www.sowhatpictures.com/wp-content/themes/sowhatpictures/img/svg/mapmarker.svg"
						});
			
						// Styling Map
						var styles = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];
						map.setOptions({styles: styles});
					}
					google.maps.event.addDomListener(window, 'resize', initialize);
					google.maps.event.addDomListener(window, 'load', initialize)
				});
			});
			
		</script>
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>