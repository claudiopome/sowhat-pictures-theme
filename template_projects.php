<?php
/*
Template Name: Page - Projects
*/
?>
<?php get_header(); ?>
		
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="projects-home">
						<!--
						<header class="section-intro--small section--black text-center">
							<h6 class="separator">Our work</h6>
							<h1>Some other headline</h1>
						</header>
						-->
						<section class="section--blue text-center caps" style="padding: 20px 0;">Filter projects by</section>
						<nav class="projects-nav">
							<ul>
								
								<?php $terms = get_terms("projects_cat", "order=ASC&hide_empty=0&exclude=16,17,29");
								$count = count($terms);
								if ( $count > 0 ){
								
									foreach ( $terms as $term ) {
										echo '<li><a href="' . get_term_link($term->slug, 'projects_cat').'">' . $term->name . '</a></li>';        
									}
							     } 
						   ?>
							</ul>	
						</nav>
					
					<ul class="projects-list">
					<?php 
					$args = array(
						'post_type' => 'projects',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'projects_cat',
								'field' => 'slug',
								'terms' => array('featured'),
								'operator' => 'NOT_IN'
							),
						),
					);
					$other = new WP_Query($args);
					while($other -> have_posts()) : $other -> the_post(); 
					$post_image = sowhatpictures_theme_fetch_post_image(); ?>
					
					<li class="projects_page__item grid__item one-third">
						<a href="<?php the_permalink(); ?>" class="projects__link">
							<div class="projects__content">
								<img class="filter_effect" src="<?php echo $post_image; ?>" alt="Project">
							 	<div class="projects_page__overlay">
									<div class="projects__heading">
										<?php
											$url = $_SERVER['REQUEST_URI'];
											$output = "";
											if (substr($url,-9)=="/projects" || substr($url,-10)=="/projects/")
											{
												$term_list = wp_get_post_terms($post->ID, 'projects_cat', array("fields" => "all"));
											
												for ($i = 0; $i < count($term_list); $i++) {
													if ($output) {
														$output .= ", ";
													}
													$output .=  $term_list[$i]->name;
												}
											}
											else
											{	
												$output =  "featured";
											}
											echo ' <h6 class="projects__cat separator">' . $output . '</h6>';
										?>
											<h1 class="projects__title"><?php the_title(); ?></h1>
									</div>
								</div>
							</div>
						</a>
					
					</li>
					<?php endwhile; ?>
					

					</ul>
				</section>
				
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>Like what you see?</h1>
							<ul class="button-list">
								<li>
									<a href="mailto:crew@sowhatpictures.com" class="btn">Contact us</a>
								</li>
								
							</ul>
						
					</div>
				</section>		
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>