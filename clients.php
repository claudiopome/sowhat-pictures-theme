<?php
/*
Template Name: Page - Clients
*/
?>
<?php get_header(); ?>
		<main class="site-content" role="main">
			
			<div class="inner-content">
			<section class="grid">
				<section class="projects-home">
						
					
					<ul class="projects-list">
						<li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/abbott.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/acea.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/alfa-romeo.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/alitalia.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/barilla.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/enel.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/fca.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/fox.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/leoburnett.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/luiss.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/magnum.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/national-geographic.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/nissan.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/pfizer.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/pm.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/publicis.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/repubblica.jpg" alt="Project">
								</div>
						</li><li class="projects__item grid__item one-third">
								<div class="projects__content">
									<img src="img/clients/jpg/universal.jpg" alt="Project">
								</div>
						</li>			
					</ul>
				</section>
				
				<section class="section-bottom section-cta section--blue text-center">
					<div class="inner-bottom">
						<h1>Wanna be one of them?</h1>
							<ul class="button-list">
								<li>
									<a href="mailto:crew@sowhatpictures.com" class="btn">Let's work together</a>
								</li>
								
							</ul>
						
					</div>
				</section>		
			</section> <!--/grid -->
		</main>
		
		
		</div> <!-- /inner-content -->
<?php get_footer(); ?>