<?php 
	
	/* THEME CUSTOM POST TYPES
	   --------------------------------------------------------------------------------- */
	    
	    add_action('init', 'sowhatpictures_theme_create_post_types');
	    
	    function sowhatpictures_theme_create_post_types() {
	    
	      
		     register_post_type('projects', array(
		     		
		     		'labels' => array(
		     			'name' => __('Projects'),
		     			'singular_name' => __('Projects'),
		     		),
		     	 
		     		'show_ui' => true,
		     		'_builtin' => false,
		     		'capability_type' => 'post',
		    		'hierarchical' => false,
		    		'public' => true,
		    		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
		    		'taxonomies' => array('projects_cat', 'post_tag'),
		    		
		    	    )
		     );
		     
		    
		    register_post_type('clients', array(
		    		
		    		'labels' => array(
		    			'name' => __('Clients'),
		    			'singular_name' => __('Clients'),
		    			
		    		 ),	
		    		 
		    		 'show_ui' => true,
		    		 '_builtin' => false,
		    		 'capability_type' => 'post',
		    		 'hierarchical' => false,
		    		 'public' => true,
		    		 'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
		    		 'taxonomies' => array('post_tag'),
		    		 )
		       );

		}
    
        
    /* CUSTOM TAXONOMIES
       --------------------------------------------------------------------------------- */
  
      add_action('init', 'sowhatpictures_theme_create_custom_taxonomies', 0);
  
      function sowhatpictures_theme_create_custom_taxonomies() {
	  		$labels = array(
	  			'name' => _x('Projects Categories', 'taxonomy general name'),
	  			'singular_name' => _x('Projects Category', 'taxonomy singular name'),
	  			'search_items' => __('Search Projects'),
	  			'all_items' => __('All Projects Categories'),
	  			'parent_item' => __('Parent Projects Category'),
	  			'parent_item_colon' => __('Parent Projects Category'),
	  			'edit_item' => __('Edit Projects Category'),
	  			'update_item' => __('Update Projects Category'),
	  			'add_new_item' => __('Add New Projects Category'),
	  			'new_item_name' => __('New Projects Category Name'),
	  	     );
	  
	  register_taxonomy('projects_cat', array('projects'),
	  		array(
	  			'hierarchical' => true,
	  			'labels' => $labels,
	  			'show_ui' => true,
	  			'query_var' => true,
	  			'rewrite' => array('slug' => 'projects-categories')
	  		));
	   }

		 
	/* CUSTOM POST TYPES COLUMNS
	  --------------------------------------------------------------------------------- */
	  
	  add_filter('manage_edit-projects_columns', 'sowhatpictures_theme_projects_columns');
	  add_action('manage_posts_custom_column', 'sowhatpictures_theme_projects_custom_columns');
	  
	  add_filter('manage_edit-clients_columns', 'sowhatpictures_theme_clients_columns');
	  add_action('manange_posts_custom_column', 'sowhatpictures_theme_projects_custom_columns');
	  
	  function sowhatpictures_theme_projects_columns() {
		  $columns = array(
		  	'cb' => '<input type=\"checkbox\" />',
		  	"title" => __("Work"),
		  	"author" => __("Author"),
		  	"projects_cats" => __("Categories"),
		  	"date" => __("Date")
		  );
	  
	  return $columns;

	  }
	  
	  function sowhatpictures_theme_projects_custom_columns($column) {
	 	 global $post;
	  
	 	 switch ($column) {
	 	 	
	 	 			  
		 	 case "author" :
		 	 the_author();
		 	 break;
		 	 
		 	 case "projects_cats" :
		 	 echo get_the_term_list($post->ID, 'projects_cat', '', ', ', '');
		 	 break;
		 }
	  }
	  
	   function sowhatpictures_theme_clients_columns($columns) {
		   $columns = array(
		   	 "cb" => "<input type=\"checkbox\" />",
	  	     "title" => _x("Title", 'post title columns'),
	  	     "author" => _x("Author", 'post author column'),
	  	     "date" => _x("Date", 'post date column')
	      );
	  
	       return $columns;
       }
  
      function sowhatpictures_theme_clients_custom_columns($column) {
	      global $post;
	  
	      if ($column == "author") {
		     the_author();
	      }
       }
      
	/* REGISTER AND LOAD SITE SCRIPTS 
      --------------------------------------------------------------------------------- */
     
      
      function sowhatpictures_theme_register_scripts() {
	  		if(!is_admin()) {
		  		wp_deregister_script('jquery');
		  		wp_register_script('fitvids', '//cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js', false, '1.9.1', true);
		  		wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', false, '1.0', true);
		  		wp_register_script('sitescripts', get_template_directory_uri() . '/js/main-ck.js', false, '1.0', true);		  		
		  		
		  		wp_enqueue_script('fitvids');
		  		wp_enqueue_script('owl-carousel');
		  		wp_enqueue_script('sitescripts');
	  		}    
      }
      
       
      add_action('init', 'sowhatpictures_theme_register_scripts');
	
	/* SUPPORT FOR WP3 FEATURES
	  --------------------------------------------------------------------------------- */
	  
	  add_theme_support('post-thumbnails');
	  add_theme_support('nav-menus');
	  
	  if(function_exists('wp_nav_menu')) {
		  register_nav_menu('primary_nav', 'Primary Navigation');
	  }
	  
	  function sowhatpictures_theme_custom_nav_menu() {
		  if (function_exists('wp_nav_menu')) :
		  	
		  	wp_nav_menu(
		  		array(
		  			'container' => 'false',
		  			'container_id' => 'menu',
		  			'depth' => 2
		  		)
		  	);
		  	
		  	else:
		  	
		  	 echo '<ul>';
		  	 wp_list_pages('depth=1&title_li=');
		  	 echo '</ul>';
		  	
		  	endif;
	  }
	 
	   
	  /* DISPLAY PAGE TITLES
	    --------------------------------------------------------------------------------- */
	    
	    function sowhatpictures_theme_display_titles() {
		    
		    if(!$separator) {
			    $separator = " | ";
			}
			    
			if(is_front_page()) {
				 bloginfo('name'); echo $separator; bloginfo('description');
			}
			    
		    else if(is_single() or is_page() or is_home()) {
			     bloginfo('name');
				 wp_title($separator, true, '');
			}
			    
			else if(is_404()) {
				  bloginfo('name');
				  echo " $separator ";
				  echo('Page Not Found');
			}
			    
			else {
				 bloginfo('name');
				 wp_title($separator, true, '');
			}
	    }
	 
	 /* POST DEFAULT CONTENT
	   --------------------------------------------------------------------------------- */
	   
	   add_filter( 'default_content', 'sowhatpictures_theme_default_post_content', 10, 2 );

	   function sowhatpictures_theme_default_post_content( $content, $post ) {

		    switch( $post->post_type ) {
		        case 'projects':
		            $content = '[heading-separator]About the project[/heading-separator]
<p class="text-center">Client: The Space Cinema</p>

[heading-separator]Credits[/heading-separator]
<p class="text-center">Post production: Direct2Brain, Art director: Sara Taigher, Visual FX: Chromany</p>';
		        break;
		        case 'clients':
		            $content = '';
		        break;
		        default:
		            $content = '[heading-separator]About the project[/heading-separator]
<p class="text-center">Client: The Space Cinema</p>

					[heading-separator]Credits[/heading-separator]
<p class="text-center">Post production: Direct2Brain, Art director: Sara Taigher, Visual FX: Chromany</p>';
		        break;
		    }
		
		    return $content;
		}

	 /* WIDGET SIDEBARS AND FOOTER
	   --------------------------------------------------------------------------------- */
	   
	   if ( function_exists( 'register_sidebar' )) {
		   register_sidebar ( array (
			   'name' => 'Where we are',
			   'description' => 'Widget for the first footer\'s column',
			   'before_widget' => '',
			   'after_widget' => '',
			   'before_title' => '<h6 class="separator">',
			   'after_title' => '</h6>',
		   ));
		   
		   register_sidebar ( array (
			   'name' => 'Email us',
			   'description' => 'Widget for the second footer\'s column',
			   'before_widget' => '',
			   'after_widget' => '',
			   'before_title' => '<h6 class="separator">',
			   'after_title' => '</h6>',
		   ));
		   
		   register_sidebar ( array (
			   'name' => 'Call',
			   'description' => 'Widget for the third footer\'s column',
			   'before_widget' => '',
			   'after_widget' => '',
			   'before_title' => '<h6 class="separator">',
			   'after_title' => '</h6>',
		   ));
		   
		    register_sidebar ( array (
			   'name' => 'Be Social',
			   'description' => 'Widget for the fourth footer\'s column',
			   'before_widget' => '',
			   'after_widget' => '',
			   'before_title' => '<h6 class="separator">',
			   'after_title' => '</h6>',
		   ));
	   }
	 
	 /* POST IMAGE
	   --------------------------------------------------------------------------------- */
	   
	   function sowhatpictures_theme_fetch_post_image() {
	   		
	   		global $post;
	   		$image = '';
	   		
	   		//Get the image from the post meta box
	   		$image = get_post_meta($post->ID, 'sowhatpictures_post_image', true);
	   		if($image) return $image;
	   		
	   		//If the above doesn't exist, get the post thumbnail
	   		$image_id = get_post_thumbnail_id($post->ID);
	   		$image = wp_get_attachment_image_src($image_id, 'sowhatpictures_thumb');
	   		$image = $image[0];
	   		if($image) return $image;
	
	
	   		//If there is still no image, get the first image from the post
	   		return sowhatpictures_get_first_image();
	   }
	   
	 /* GETS THE FIRST IMAGE FROM THE POST
	   --------------------------------------------------------------------------------- */
	   
	   function sowhatpictures_get_first_image() {
		   
		   global $post, $posts;
		   $first_img = '';
		   ob_start();
		   ob_end_clean();
		   $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		
		   $first_img="";
		
		   if(isset($matches[1][0]))
		   		$first_img = $matches[1][0];
			
		   return $first_img;
	    }
	    
	 /* DISPLAY CUSTOM EXCERPT
	   --------------------------------------------------------------------------------- */
	   
	   function sowhatpictures_theme_custom_excerpt($len=20, $trim="&hellip;") {
		   $limit = $len+1;
		   $excerpt = explode(' ', get_the_excerpt(), $limit);
		   $num_words = count($excerpt);
		   
		   if($num_words >= $len) {
			   $last_item = array_pop($excerpt);
		   }
		   
		   else {
			   $trim = '';
		   }
		   
		   $excerpt = implode(" ",$excerpt) . "$trim";
		   echo $excerpt;
	   }	
			
	 /* POST PAGINATION LINK ATTRIBUTES
	   --------------------------------------------------------------------------------- */	
	   
	   	add_filter('next_posts_link_attributes', 'posts_link_attributes');
	   	add_filter('previous_posts_link_attributes', 'posts_link_attributes');

	   	function posts_link_attributes() {
		   	return 'class="controls-toggle"';
		}
		
	/* THEME SHORTCODES
	   --------------------------------------------------------------------------------- */
	  
	   function sowhatpictures_heading_separator_shortcode($atts, $content = null) {
		   return '<h6 class="separator text-center">' . do_shortcode($content) . '</h6>';
	   }
	   add_shortcode('heading-separator', 'sowhatpictures_heading_separator_shortcode');
	    
?>